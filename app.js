/** Created by Juan Manuel Ventura on 04-12-14 10:34. */

'use strict';

var express = require('express');
var app = express();

app.get('/', function (req, res) {
  res.send('Hello world!');
});


app.get('/test', function (req, res) {
  res.send('test route');
});



app.listen(3000);
